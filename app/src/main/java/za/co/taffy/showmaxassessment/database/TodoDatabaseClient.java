package za.co.taffy.showmaxassessment.database;

import android.arch.persistence.room.Room;
import android.content.Context;

public class TodoDatabaseClient {

    private static TodoDatabaseClient mInstance;
    private TodoDatabase todoDatabase;

    private TodoDatabaseClient(Context dbContext) {
        todoDatabase = Room.databaseBuilder(dbContext, TodoDatabase.class, "ToDoDatabase").build();
    }

    public static synchronized TodoDatabaseClient getInstance(Context dbContext) {
        if (mInstance == null) {
            mInstance = new TodoDatabaseClient(dbContext);
        }
        return mInstance;
    }

    public TodoDatabase getAppDatabase() {
        return todoDatabase;
    }
}
