package za.co.taffy.showmaxassessment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import za.co.taffy.showmaxassessment.database.TodoDatabaseClient;
import za.co.taffy.showmaxassessment.models.Note;

public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Note> noteList;
    private int itemLayout;

    public NotesRecyclerAdapter(Context context, List<Note> noteList, int itemLayout) {
        this.context = context;
        this.noteList = noteList;
        this.itemLayout = itemLayout;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int index) {

        Note note = noteList.get(index);

        viewHolder.headingTextview.setText(note.getTitle());
        viewHolder.descriptiontextview.setText(note.getDescription());
        viewHolder.completedCheckbox.setChecked(note.isTaskCompleted());

        viewHolder.setCurrentNote(note);

        viewHolder.completedCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (viewHolder.completedCheckbox.isChecked()) {
                    viewHolder.getCurrentNote().setTaskCompleted(true);
                    viewHolder.updateNote(viewHolder.getCurrentNote());
                } else {
                    viewHolder.getCurrentNote().setTaskCompleted(false);
                    viewHolder.updateNote(viewHolder.getCurrentNote());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    /* View holder */
    protected class ViewHolder extends RecyclerView.ViewHolder {

        Note currentNote;

        private TextView headingTextview;
        private TextView descriptiontextview;
        private CheckBox completedCheckbox;

        private ViewHolder(final View itemView) {
            super(itemView);

            // Setting of UI elements
            headingTextview = itemView.findViewById(R.id.to_to_heading);
            descriptiontextview = itemView.findViewById(R.id.to_do_body);
            completedCheckbox = itemView.findViewById(R.id.completed_checkbox);

            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openNoteDetailsScreen(getCurrentNote());
                        }
                    }
            );
        }

        private Note setCurrentNote(Note todoNote) {
            this.currentNote = todoNote;
            return todoNote;
        }

        public Note getCurrentNote() {
            return this.currentNote;
        }

        private void openNoteDetailsScreen(Note currentNote) {
            Intent openNoteDetails = new Intent(context, NoteDetailsActivity.class);
            openNoteDetails.putExtra("currentNote", currentNote);
            context.startActivity(openNoteDetails);
        }

        /* Update note completion status */
        public void updateNote(final Note todoNote) {
            class UpdateTodoNote extends AsyncTask<Void, Void, Void> {

                @Override
                protected Void doInBackground(Void... voids) {
                    todoNote.setTaskCompleted(todoNote.isTaskCompleted());
                    TodoDatabaseClient.getInstance(context).getAppDatabase()
                            .todoDao()
                            .updateTodoNote(todoNote);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    if (completedCheckbox.isChecked()) {
                        Toast.makeText(context, "Marked as Complete", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Marked as Incomplete", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            UpdateTodoNote updateTodoNote = new UpdateTodoNote();
            updateTodoNote.execute();
        }
    }
}