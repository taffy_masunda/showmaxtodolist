package za.co.taffy.showmaxassessment.database;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import za.co.taffy.showmaxassessment.models.Note;

@Dao
public interface TodoTaskDAO {

    @Insert
    void insertTodoNOte(Note todoNote);

    @Query("SELECT * FROM note")
    List<Note> getAll();

    @Delete
    void deleteTodoNote(Note todoNote);

    @Update
    void updateTodoNote(Note todoNote);
}
