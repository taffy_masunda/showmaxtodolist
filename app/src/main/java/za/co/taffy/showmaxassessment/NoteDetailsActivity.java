package za.co.taffy.showmaxassessment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import za.co.taffy.showmaxassessment.database.TodoDatabaseClient;
import za.co.taffy.showmaxassessment.models.Note;

public class NoteDetailsActivity extends AppCompatActivity {

    private Note currentNote;
    TextView completeStatusTextview, titleTextview, descriptionTextview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        currentNote = (Note) getIntent().getSerializableExtra("currentNote");
        setupViews(currentNote);

        // delete action
        FloatingActionButton delete_fab = findViewById(R.id.delete_fab);
        delete_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmDelete();
            }
        });
    }

    public void setupViews(Note currentNote) {
        completeStatusTextview = findViewById(R.id.complete_status_textview);
        titleTextview = findViewById(R.id.heading_textview);
        descriptionTextview = findViewById(R.id.description_textview);

        getStatusText(currentNote);
        titleTextview.setText(currentNote.getTitle());
        descriptionTextview.setText(currentNote.getDescription());
    }

    public void getStatusText(Note currentNote) {
        String statusText = getString(R.string.pending_status);
        completeStatusTextview.setTextColor(Color.RED);

        if (currentNote.isTaskCompleted()) {
            statusText = getString(R.string.completed_status);
            completeStatusTextview.setTextColor(Color.BLUE);
        }

        completeStatusTextview.setText(statusText);
    }

    public void confirmDelete() {
        new AlertDialog.Builder(this)
                .setTitle("Confirm Delete")
                .setMessage("Are you sure you want to delete this To-do note?")
                .setIcon(android.R.drawable.ic_delete)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteToDoTask();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    /* Delete note from Database */
    private void deleteToDoTask() {

        class DeleteTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                TodoDatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .todoDao()
                        .deleteTodoNote(currentNote);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(NoteDetailsActivity.this, "To-do note deleted", Toast.LENGTH_SHORT).show();
                finish();
                Intent openMainList = new Intent(NoteDetailsActivity.this, MainActivity.class);
                startActivity(openMainList);
            }
        }

        DeleteTask dt = new DeleteTask();
        dt.execute();
    }
}
