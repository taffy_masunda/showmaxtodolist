package za.co.taffy.showmaxassessment.database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import za.co.taffy.showmaxassessment.models.Note;

@Database(entities = {Note.class}, version = 1, exportSchema = false)
public abstract class TodoDatabase extends RoomDatabase {

    public abstract TodoTaskDAO todoDao();

}