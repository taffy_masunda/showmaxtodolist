package za.co.taffy.showmaxassessment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import za.co.taffy.showmaxassessment.database.TodoDatabaseClient;
import za.co.taffy.showmaxassessment.models.Note;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    TextView noNotesTextview;

    ProgressBar progressBar;
    TextView completedPercTextview;

    RecyclerView notesRecyclerview;
    NotesRecyclerAdapter notesRecyclerAdapter;

    Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.to_do_list);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        // app setup
        setupViews();
        getNotes();
    }

    public void setupViews() {
        noNotesTextview = findViewById(R.id.no_items_textview);
        notesRecyclerview = findViewById(R.id.toddo_recycler_view);

        progressBar = findViewById(R.id.completion_progress_bar);
        completedPercTextview = findViewById(R.id.complete_percentage_textview);

        addButton = findViewById(R.id.add_button);

        // Action listeners
        addButton.setOnClickListener(this);
    }

    public void setupRecycler(List<Note> noteList) {
        if (noteList.size() > 0) {

            notesRecyclerview.setVisibility(View.VISIBLE);
            noNotesTextview.setVisibility(View.INVISIBLE);

            notesRecyclerAdapter = new NotesRecyclerAdapter(this, noteList, R.layout.to_do_list_item_layout);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

            notesRecyclerview.setLayoutManager(layoutManager);
            notesRecyclerview.setAdapter(notesRecyclerAdapter);
        } else {

            notesRecyclerview.setVisibility(View.INVISIBLE);
            noNotesTextview.setVisibility(View.VISIBLE);
        }
    }

    public void updateToDoProgress(List todoList) {
        float totalToDos = todoList.size();
        int totalCompletedToDos = 0;

        for (int i = 0; i < todoList.size(); i++) {
            Note currentNote = (Note) todoList.get(i);
            if (currentNote.isTaskCompleted()) {
                totalCompletedToDos += 1;
            }
        }

        float toDoProgress = (totalCompletedToDos / totalToDos) * 100;
        int progress = (int) toDoProgress;

        progressBar.setProgress(progress);
        completedPercTextview.setText(progress + "%");

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_exit) {
            exitApp();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_add) {
            openTodoInput();
        } else if (id == R.id.nav_exit) {
            exitApp();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void exitApp() {
        Toast.makeText(this, "Closing app...", Toast.LENGTH_SHORT).show();
        System.exit(0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_button:
                openTodoInput();
                break;
        }
    }

    public void openTodoInput() {

        LayoutInflater li = LayoutInflater.from(this);
        final View inputView = li.inflate(R.layout.to_do_input_layout, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(inputView);

        // setting dialog action
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton("Add New To-do",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                saveNewTodo(inputView);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /* Get all tasks */
    public void getNotes() {

        class GetTodoNotes extends AsyncTask<Void, Void, List<Note>> {

            @Override
            protected List<Note> doInBackground(Void... voids) {
                List<Note> todoList = TodoDatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .todoDao()
                        .getAll();

                return todoList;
            }

            @Override
            protected void onPostExecute(List<Note> allNotes) {
                super.onPostExecute(allNotes);
                setupRecycler(allNotes);
                updateToDoProgress(allNotes);
            }
        }

        GetTodoNotes getTodoNotes = new GetTodoNotes();
        getTodoNotes.execute();

    }

    /* Save note to Database */
    public void saveNewTodo(View view) {
        EditText titleEditText = view.findViewById(R.id.title_edittext);
        EditText descriptionEdittext = view.findViewById(R.id.description_edittext);

        final String title = titleEditText.getText().toString();
        final String description = descriptionEdittext.getText().toString();

        class SaveTodoNote extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                Note newTodoNote = new Note();
                newTodoNote.setTitle(title);
                newTodoNote.setDescription(description);
                newTodoNote.setTaskCompleted(false);

                TodoDatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .todoDao()
                        .insertTodoNOte(newTodoNote);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                finish();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                Toast.makeText(getApplicationContext(), "To-do Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveTodoNote saveTodoNote = new SaveTodoNote();
        saveTodoNote.execute();
    }

}
